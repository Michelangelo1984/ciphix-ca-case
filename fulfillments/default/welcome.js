/**
 * Intent: Default Welcome Intent
 * Fulfillment: default
 */

module.exports = {

  fulfillment: function (agent) {
    agent.add('Hello there roamer of the sky, ask me about the weather')
  }

}
